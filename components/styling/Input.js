import React from 'react'
import PropTypes from 'prop-types'
import { TextInput, StyleSheet } from 'react-native';
import Colors from '../../constants/Colors';
const Input = props => {
    return (
        <TextInput {...props} style={{...styles.input, ...props.style}} />
    )
}

const styles = StyleSheet.create({
    input: {
        height: 30,
        borderBottomColor: Colors.bottomBorder,
        borderBottomWidth: 0.7,
        marginVertical: 12
    }
})

export default Input
