import React from 'react'
import { StyleSheet, View } from 'react-native';

const Cards = props => {
    return (
        <View style={{...styles.card, ...props.style}}>{props.children}</View>
    )
}


const styles = StyleSheet.create({
    card: {
        // shadowColor: 'black',
        // shadowOffset: { width: 0, height: 2 },
        // shadowRadius: 5,
        // shadowOpacity: 0.25,
        elevation: 5,
        backgroundColor: 'white',
        padding: 20
    },
})
export default Cards
