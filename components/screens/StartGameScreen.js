import React, { useState } from 'react'
import { View, Text, StyleSheet, TextInput, Button, TouchableWithoutFeedback, Keyboard, Alert } from 'react-native';
import Cards from '../styling/Cards';
import Colors from '../../constants/Colors';
import Input from '../styling/Input';
import NumberContainer from '../styling/NumberContainer';
const StartGameScreen = props => {
    const [ enteredValue, setEnteredValue ] = useState('');

    const [confirmed, setConfirmed] = useState(false)

    const [ selectedNumber, setSelectedNumber ] = useState();

    const numberInputHandler = inputText => {
        setEnteredValue(inputText.replace(/[^0-9]/g, ''));
    }

    const resetInputHandler = () => {
        setEnteredValue('');
        setConfirmed(false);
    }

    const confirmInputHandler = () => {
        const chosenNumber = parseInt(enteredValue);
        if(isNaN(chosenNumber) || chosenNumber <= 0 || chosenNumber > 99) {
            Alert.alert('Invalid Input !', 'The input should be a number and should be in between 1 to 99', [{text: 'Okay', style: 'destructive', onPress: resetInputHandler}])
            return;
        }
        setConfirmed(true);
        setSelectedNumber(parseInt(chosenNumber))
        setEnteredValue('');
        Keyboard.dismiss();
    }

    let confirmedOutput;

    if(confirmed) {
        confirmedOutput = 
        <Cards style={styles.summaryContainer}>
        <Text>You Selected</Text>
        <NumberContainer>{selectedNumber}</NumberContainer>
        <Button title="Start Game !" onPress={() => props.onStartGame(selectedNumber)} />
        </Cards>
    }
    return (
        <TouchableWithoutFeedback onPress={() => {
            Keyboard.dismiss();
        }}>
        <View style={styles.screen}>
            <Text style={styles.title}>Start a New Game</Text>
            <Cards style={styles.inputContainer}>
                <Text>Select a Number</Text>
                <Input 
                 blurOnSubmit autoCapitalize='none' 
                autoCorrect={false}
                keyboardType='number-pad' maxLength={2} 
                style={styles.input}
                value={enteredValue}
                onChangeText={numberInputHandler}
                />
                <View style={styles.buttonContainer}>
                <View style={styles.button}>
                    <Button title='Reset' color={Colors.secondary} onPress={resetInputHandler}/>
                    </View>
                    <View style={styles.button}>
                    <Button title='Confirm' color={Colors.primary}
                        onPress={confirmInputHandler}
                    />
                    </View>
                </View>
            </Cards>
            {confirmedOutput}
        </View>
        </TouchableWithoutFeedback>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10,
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
        marginVertical: 14
    },
    buttonContainer: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        paddingHorizontal: 14
    },
    inputContainer: {
        width: 300,
        maxWidth: '80%',
        alignItems: 'center'
    },
    button: {
        width: 100
    },
    input: {
        width: 50,
        textAlign: 'center'
    },
    summaryContainer: {
        marginTop: 20,
        alignItems: 'center'
    }
})

export default StartGameScreen
