import React, { useState, useRef, useEffect } from 'react'
import { View, Text, StyleSheet, Button, Alert } from 'react-native';
import NumberContainer from '../styling/NumberContainer';
import Cards from '../styling/Cards';
const generateRandomBetween = (min, max, exclude) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    const random = Math.floor(Math.random() * (max - min) + min);
    // Calling the same function inside the funciton is called recursion.
    if(random === exclude) {
        return generateRandomBetween(min, max, exclude);
    }
    else {
        return random;
    }
}

const GameScreen = props => {
    const [ currentGuess, setCurrentGuess ] = useState(generateRandomBetween(1, 100, props.userChoice));

    const [ rounds, setRounds ] = useState(0);

    const currentLow = useRef(1);
    const currentHigh = useRef(100);

    const { userChoice, onGameOver } = props;

    useEffect(() => {
        if(currentGuess === userChoice) {
            onGameOver(rounds);
        }
    }, [onGameOver, userChoice, currentGuess])

    const nextGuessHandler = (direction) => {
        if((direction === 'lower' && currentGuess < props.userChoice) || (direction === 'greater' && currentGuess > props.userChoice )) {
            Alert.alert('Don\'t Lie Nigga !', 'You know that this is wrong...', [{text: 'Sorry !', style: 'cancel'}])
            return;
        }

        if(direction === 'lower') {
            currentHigh.current = currentGuess;
        }
        else {
            currentLow.current = currentGuess;
        }
        const nextNumber = generateRandomBetween(currentLow.current, currentHigh.current, currentGuess);
        setCurrentGuess(nextNumber);
        setRounds(curRound => curRound + 1);
    }
    return (
       <View style={styles.screen}>
           <Text>Opponent's Guess</Text>
           <NumberContainer>{currentGuess}</NumberContainer>
           <Cards style={styles.buttonContainer}>
               <Button title="Lower" onPress={() => {nextGuessHandler('lower')}} />
               <Button title="Greater" onPress={() => {nextGuessHandler('greater')}} />
           </Cards>
       </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10,
        alignItems: 'center'
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 20,
        width: 300,
        maxWidth: '80%'
    }
})

export default GameScreen;
