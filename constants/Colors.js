export default {
    primary: '#00bfff',
    secondary: '#191970',
    bottomBorder: '#b0e0e6'
}