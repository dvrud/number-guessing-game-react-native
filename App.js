import React, {useState} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Header from './components/Header';
import StartGameScreen from './components/screens/StartGameScreen';
import GameScreen from './components/screens/GameScreen';
import GameOver from './components/screens/GameOver';
export default function App() {
  const [ userNumber, setUserNumber ] = useState();

  const [ guessRounds, setGuessRounds ] = useState(0);

  const gameOverHandler = (numOfRounds) => {
    setGuessRounds(numOfRounds)
  }
  const startGameHandler = (selectedNumber) => {
    setUserNumber(selectedNumber);
    setGuessRounds(0);
  };

  let content = <StartGameScreen onStartGame={startGameHandler}/>;

  if(userNumber && guessRounds <= 0) {
    content = <GameScreen onGameOver= {gameOverHandler} userChoice={userNumber} />
  } else if(guessRounds > 0) {
    content = <GameOver />
  }
  return (
    <View style={styles.screen}>
      <Header title="Guess A Number" />
      {content}
      {/* <StartGameScreen />
      <GameScreen /> */}
    </View>
  );
}

const styles = StyleSheet.create({
 screen: {
   flex: 1
 }
});
